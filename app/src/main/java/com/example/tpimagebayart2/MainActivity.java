package com.example.tpimagebayart2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {
    private Bitmap initialPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = findViewById(R.id.imageView);
        registerForContextMenu(imageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_context, menu);
    }

    /**
     * Méthode permettant de charger une image dans l'application
     *
     * @param view : bouton cliqués
     */
    public void chargerImage(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // --- on démarre une activité qui attend un résultat (ici l'image choisie) --- //
        startActivityForResult(Intent.createChooser(intent, "Choose Picture"), 1);
    }

    /**
     * Méthode appellée suite à la selection d'une image
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // --- Dans le cas où une image a été selectionnée --- //
        if (resultCode == RESULT_OK && requestCode == 1) {
            // --- Récupération de l'URI de l'image selectionnée --- //
            Uri selectedimg = data.getData();
            ImageView imageView = findViewById(R.id.imageView);

            // --- On affiche l'URI de l'image dans le TextView --- //
            TextView urlView = findViewById(R.id.urlView);

            if(selectedimg != null)
                urlView.setText(selectedimg.toString());


            // ----- préparer les options de chargement de l’image
            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inMutable = true; // l’image pourra être modifiée
            // ------ chargement de l’image - valeur retournée null en cas d’erreur

            try {
                if(selectedimg != null)
                    initialPicture = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedimg), null, option);
                // --- On applique l'image selectionnée à l'ImageView de l'application --- //
                imageView.setImageBitmap(initialPicture);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Méthode appelée en cas d'intéraction avec le menu de l'application
     *
     * @param item : item cliqué
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        ImageView image = findViewById(R.id.imageView);
        // --- On stocke l'image courante --- //
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        Bitmap bitmapChanged = Bitmap.createBitmap(bitmap);

        switch(item.getItemId()) {
            case R.id.horizontalMirror:
                // --- Dans le cas du clic sur le bouton "Miroir Horizontal", on échange les pixels de l'image "bitmapChanged" à l'aide de ceux de l'image courante" --- //
                bitmapChanged = horizontalFLip(bitmap);
                break;
            case R.id.verticalMirror:
                // --- Dans le cas du clic sur le bouton "Miroir Vertical", on échange les pixels de l'image "bitmapChanged" à l'aide de ceux de l'image courante" --- //
                bitmapChanged = verticalFlip(bitmap);
                break;
            case R.id.rotatePos:
                // --- Dans le cas du clic sur le bouton "Rotation 90° horaire", on fait pivoter l'image de 90° dans le sens horaire --- //
                bitmapChanged = rotateBitmap(bitmap, 90);
                break;
            case R.id.rotateNeg:
                // --- Dans le cas du clic sur le bouton "Rotation 90° anti-horaire", on fait pivoter l'image de 90° dans le sens anti-horaire --- //
                bitmapChanged = rotateBitmap(bitmap, -90);
                break;
            default:
                break;
        }

        // --- On affiche l'image modifiée dans l'ImageView --- //
        image.setImageBitmap(bitmapChanged);
        return super.onOptionsItemSelected(item);
    }

    /**
     * Méthode appelée en cas d'intéraction avec le menu contextuel de l'ImageView
     *
     * @param item : item cliqué
     */
    //gère le click sur une action de l'ActionBar
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        ImageView image = findViewById(R.id.imageView);
        // --- On stocke l'image courante --- //
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        Bitmap bitmapChanged = Bitmap.createBitmap(bitmap);

        switch(item.getItemId()) {
            case R.id.invertColors:
                // --- Dans le cas du clic sur le bouton "Inverser les couleurs", on inverse les couleurs de l'image "bitmapChanged" à l'aide de ceux de l'image courante" --- //
                bitmapChanged = invertImageColors(bitmap);
                break;
            case R.id.transformGrey:
                // --- Dans le cas du clic sur le bouton "Transformer en niveaux de gris (Moyenne)", on transforme en niveau de gris les pixels de l'image "bitmapChanged" à l'aide de ceux de l'image courante" --- //
                bitmapChanged = transformToGreyMean(bitmap, 1);
                break;
            case R.id.transformGrey2:
                // --- Dans le cas du clic sur le bouton "Transformer en niveaux de gris (Min/Max)", on transforme en niveau de gris les pixels de l'image "bitmapChanged" à l'aide de ceux de l'image courante" --- //
                bitmapChanged = transformToGreyMean(bitmap, 2);
                break;
            case R.id.transformGrey3:
                // --- Dans le cas du clic sur le bouton "Transformer en niveaux de gris (Calculée)", on transforme en niveau de gris les pixels de l'image "bitmapChanged" à l'aide de ceux de l'image courante" --- //
                bitmapChanged = transformToGreyMean(bitmap, 3);
                break;
            default:
                break;
        }
        // --- On affiche l'image modifiée dans l'ImageView --- //
        image.setImageBitmap(bitmapChanged);
        return super.onOptionsItemSelected(item);
    }

    /**
     * Méthode permettant de flip horizontalement une image
     *
     * @param init : image sur laquelle on applique la modification
     * @return l'image "flipped"
     */
    public Bitmap horizontalFLip(Bitmap init) {
        // --- On crée une image "flipped" qui sera le résultat de l'effet miroir --- //
        Bitmap flipped = Bitmap.createBitmap(init);

        for(int y = 0; y < init.getHeight(); y++) {
            for(int x = 0; x < init.getWidth(); x++) {
                // --- On échange le pixel avec celui qui est à l'opposé horizontalement --- //
                flipped.setPixel((init.getWidth()-1) - x, y, init.getPixel(x, y));
            }
        }
        return flipped;
    }

    /**
     * Méthode permettant de flip verticalement une image
     *
     * @param init : image sur laquelle on applique la modification
     * @return l'image "flipped"
     */
    public Bitmap verticalFlip(Bitmap init) {
        // --- On crée une image "flipped" qui sera le résultat de l'effet miroir --- //
        Bitmap flipped = Bitmap.createBitmap(init);

        for(int y = 0; y < init.getHeight(); y++) {
            for(int x = 0; x < init.getWidth(); x++) {
                // --- On échange le pixel avec celui qui est à l'opposé verticalement --- //
                flipped.setPixel(x, (init.getHeight()-1) - y, init.getPixel(x, y));
            }
        }
        return flipped;
    }

    /**
     * Méthode permettant d'inverser les couleurs d'une image
     *
     * @param init : image sur laquelle on applique la modification
     * @return l'image "inverted"
     */
    public Bitmap invertImageColors(Bitmap init) {
        // --- On crée une image "inverted" qui sera le résultat de l'inversion des couleurs --- //
        Bitmap inverted = Bitmap.createBitmap(init);
        int A, R, G, B;

        for(int y = 0; y < init.getHeight(); y++) {
            for(int x = 0; x < init.getWidth(); x++) {
                // --- On inverse la couleur du pixel --- //
                A = Color.alpha(init.getPixel(x, y));
                R = 255 - Color.red(init.getPixel(x, y));
                G = 255 - Color.green(init.getPixel(x, y));
                B = 255 - Color.blue(init.getPixel(x, y));

                // --- On applique la modification de couleur à l'image --- /
                inverted.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }
        return inverted;
    }

    /**
     * Méthode permettant de les couleurs d'une image en niveau de gris
     *
     * @param init : image sur laquelle on applique la modification
     * @return l'image "grey"
     */
    public Bitmap transformToGreyMean(Bitmap init, int option) {
        // --- On crée une image "grey" qui sera l'image en niveau de gris --- //
        Bitmap grey = Bitmap.createBitmap(init);
        int A, R, G, B;
        double M;

        for(int y = 0; y < init.getHeight(); y++) {
            for(int x = 0; x < init.getWidth(); x++) {
                // --- On passe le pixel en niveau de gris --- //
                A = Color.alpha(init.getPixel(x, y));

                R = Color.red(init.getPixel(x, y));
                G = Color.green(init.getPixel(x, y));
                B = Color.blue(init.getPixel(x, y));

                switch(option) {
                    case 1:
                        // --- On calcule la moyenne des couleurs --- /
                        M = (double) (R + G + B) / 3;
                        // --- On applique la modification de couleur à l'image --- /
                        grey.setPixel(x, y, Color.argb(A, (int) M, (int) M, (int) M));
                        break;
                    case 2:
                        // --- On calcule la moyenne des min/max des couleurs --- /
                        M = (double) (max(R, G, B) + min(R, G, B))/2;
                        // --- On applique la modification de couleur à l'image --- /
                        grey.setPixel(x, y, Color.argb(A, (int) M, (int) M, (int) M));
                        break;
                    case 3:
                        // --- On calcule les nouvelles couleurs --- /
                        M = (0.21*R) + (0.72*G) + (0.07*B);
                        // --- On applique la modification de couleur à l'image --- /
                        grey.setPixel(x, y, Color.argb(A, (int) M, (int) M, (int) M));
                        break;
                    default:
                        break;
                }

            }
        }
        return grey;
    }

    /**
     * Méthode permettant de restaurer l'image à son état d'origine
     *
     * @param view : bouton cliqué
     */
    public void restaurer(View view) {
        Button reinit = (Button) view;
        ImageView image = findViewById(R.id.imageView);

        // --- On réaffecte l'image d'origine à l'ImageView --- /
        if(reinit == findViewById(R.id.reinit)) {
            image.setImageBitmap(initialPicture);
        }
    }

    /**
     * Méthode qui retourne la valeur la plus grande
     */
    public int max(int a, int b, int c) {
        int maxValue = Math.max(a, b);
        return Math.max(maxValue, c);
    }

    /**
     * Méthode qui retourne la valeur la plus grande
     */
    public int min(int a, int b, int c) {
        int maxValue = Math.min(a, b);
        return Math.min(maxValue, c);
    }

    /**
     * Méthode permettant une rotation de l'image
     *
     * @param source : image sur laquelle on applique la modification
     * @param angle : angle de rotation de l'image
     * @return l'image ayant subi une rotation
     */
    public Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        // --- On effectue une rotation à partir de l'angle passé en paramètres --- /
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
}